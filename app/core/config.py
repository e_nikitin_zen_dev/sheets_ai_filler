from functools import lru_cache

from pydantic import BaseSettings


class Settings(BaseSettings):
    CREDENTIALS_FILE: str = '../../creds.json'

    BOT_URL: str
    BOT_TOKEN: str
    BOT_MAX_TOKENS: int = 2049
    BOT_MODEL: str

    class Config:
        case_sensitive = True
        env_file = '.env'


@lru_cache(maxsize=24)
def get_settings() -> Settings:
    '''
    получение и экширование настроек проекта.
    Можно использовать как зависиомсть внутри эндпоинта
    '''
    return Settings()


settings = get_settings()

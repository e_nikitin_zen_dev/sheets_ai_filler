from fastapi import HTTPException
from fastapi.exceptions import RequestValidationError
from pydantic import BaseModel
from starlette.responses import JSONResponse


def http_exception_handler(_, exc: HTTPException):
    # if settings.SENTRY and exc.status_code != 400:
    #     sentry_sdk.capture_exception(exc)
    return JSONResponse(
        status_code=exc.status_code,
        content={"message": exc.detail}
    )


def validation_exception_handler(_, exc: RequestValidationError):
    return JSONResponse(
        status_code=400,
        content={
            'error_code': 'validation_error',
            'error_message': f'Неправильные параметры запроса: {exc}'
        }
    )


class ErrorMessage(BaseModel):
    error_code: str
    error_message: str


api_error_responses = {
    400: {'model': ErrorMessage},
    422: {'model': ErrorMessage},
}

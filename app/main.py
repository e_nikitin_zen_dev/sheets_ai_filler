from fastapi import FastAPI, HTTPException
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware

from app.api.router import router
from app.exceptions.handlers import http_exception_handler, \
    validation_exception_handler, api_error_responses


def get_application() -> FastAPI:
    openapi_url = '/openapi.json'
    app = FastAPI(
        openapi_url=openapi_url,
        swagger_ui_parameters={"defaultModelsExpandDepth": -1},
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=['*'],
        allow_methods=['*'],
        allow_headers=['*'],
        allow_credentials=True,
    )

    app.exception_handler(HTTPException)(http_exception_handler)
    app.exception_handler(RequestValidationError)(validation_exception_handler)

    app.include_router(router, responses=api_error_responses)
    return app

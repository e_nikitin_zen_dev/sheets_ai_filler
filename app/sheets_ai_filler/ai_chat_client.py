import time
from copy import copy

import requests
import json


class ChatClient:
    def __init__(self, url: str, token: str, max_tokens: int, model: str):
        self.url = url
        self.headers = {
            'Content-Type': 'application/json',
            'Authorization': token,
        }
        self.base_body = {
            'prompt': '',
            'model': model,
            'max_tokens': max_tokens
        }

    def request(self, body_text: str):
        request_body = copy(self.base_body)
        request_body['prompt'] = body_text

        response = requests.post(
            self.url,
            headers=self.headers,
            data=json.dumps(request_body)
        )
        time.sleep(1)
        try:
            return response.json()['choices'][0]['text']
        except Exception as exc:
            print(f'Exception: {exc}')
            return 'Произошла ошибка при получении данных от бота'

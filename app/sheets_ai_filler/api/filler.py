from fastapi import APIRouter, Depends, Body, Form
from starlette import status
from starlette.responses import JSONResponse as Response

from app.core.config import get_settings, Settings
from app.sheets_ai_filler.ai_chat_client import ChatClient
from app.sheets_ai_filler.schemas import InputFillData
from app.sheets_ai_filler.sheet_filler import GoogleWorkSheetFiller

router = APIRouter()


@router.post(
    path='/send',
    status_code=status.HTTP_200_OK,
)
async def run_convertation(
    settings: Settings = Depends(get_settings),
    input_data: InputFillData = Depends(InputFillData.as_form),
):

    ai_chat_client = ChatClient(
        url=settings.BOT_URL,
        token=settings.BOT_TOKEN,
        max_tokens=settings.BOT_MAX_TOKENS,
        model=settings.BOT_MODEL,
    )
    worksheet_filler = GoogleWorkSheetFiller(
        settings.CREDENTIALS_FILE,
        ai_chat_client=ai_chat_client,

    )
    processed_responses = worksheet_filler.write(
        spreadsheet_id=input_data.spreadsheet_id,
        request_column=input_data.request_column_letter,
        response_column=input_data.response_column_letter,
        start_row=input_data.check_start_row,
        check_column_to_stop=input_data.check_processing_stop_column,
    )

    return Response(
        status_code=200,
        content={"status": "ok", "processed_responses": processed_responses}
    )

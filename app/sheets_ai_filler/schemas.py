import json

from pydantic import BaseModel

from app.api.deps import as_form


@as_form
class InputFillData(BaseModel):
    check_start_row: int = 2
    spreadsheet_id: str
    request_column_letter: str
    response_column_letter: str
    check_processing_stop_column: str

    # @classmethod
    # def __get_validators__(cls):
    #     yield cls.validate_to_json
    #
    # @classmethod
    # def validate_to_json(cls, value):
    #     if isinstance(value, str):
    #         return cls(**json.loads(value))
    #     return value

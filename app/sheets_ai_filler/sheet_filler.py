from typing import List

import apiclient
import httplib2
from oauth2client.service_account import ServiceAccountCredentials

from app.sheets_ai_filler.ai_chat_client import ChatClient


class GoogleWorkSheetFiller:
    def __init__(self, creds_file: str, ai_chat_client: ChatClient):
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            creds_file,
            [
                'https://www.googleapis.com/auth/spreadsheets',
                'https://www.googleapis.com/auth/drive',
            ]
        )
        httpAuth = credentials.authorize(httplib2.Http())
        self.service = apiclient.discovery.build('sheets', 'v4', http=httpAuth)
        self.ai_chat_client = ai_chat_client


    def read_from_column(self):
        pass

    def write_responses_to_column(
            self,
            spreadsheet_id: str,
            values: List[str],
            column_letter: str,
            start_pos: int
    ):
        values_list = [[elem] for elem in values]

        start_range = f'{column_letter}{start_pos}'
        end_range = f'{column_letter}{len(values) + start_pos}'
        self.service.spreadsheets().values().batchUpdate(
            spreadsheetId=spreadsheet_id,
            body={
                'valueInputOption': 'USER_ENTERED',
                'data': [
                    {
                        "range": f'{start_range}:{end_range}',
                        "majorDimension": 'ROWS',
                        "values": values_list
                    }
                ]
            },
        ).execute()

    def get_values(
            self,
            spreadsheet_id,
            request_range: str,
    ):

        return self.service.spreadsheets().values().get(
            spreadsheetId=spreadsheet_id,
            range=request_range,
            majorDimension='ROWS',
        ).execute()['values']

    def write(
            self,
            spreadsheet_id: str,
            request_column: str,
            response_column: str,
            start_row: int,
            check_column_to_stop: str,
    ) -> int:

        req_range = f'{request_column}{start_row}:{response_column}'
        values = self.get_values(spreadsheet_id, req_range)

        responses = []
        processed_requests: int = 0

        for ind, value in enumerate(values, start_row):
            if len(value) == 0:
                extra_check_range = f'{check_column_to_stop}{ind + 1}'
                next_extra_value = self.service.spreadsheets().values().get(
                    spreadsheetId=spreadsheet_id,
                    range=f'{extra_check_range}:{extra_check_range}',
                ).execute()
                if 'values' not in next_extra_value:
                    break
                else:
                    responses.append('')
            else:
                request_to_bot = value[0]
                response = self.ai_chat_client.request(request_to_bot)
                processed_requests += 1
                responses.append(response)

            self.write_responses_to_column(
                spreadsheet_id=spreadsheet_id,
                values=responses,
                column_letter=response_column,
                start_pos=ind
            )
            responses = []

        return processed_requests